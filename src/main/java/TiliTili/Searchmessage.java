package TiliTili;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Searchmessage {

	Admin admin = new Admin();
	private String userid;
	String name;
	String ID;
	String sex;
	String age;
	String birthday; 
	void setUserid(String userid) {
		this.userid = userid;
	}
	
	String getUserid(String userid) {
		return this.userid;
	}
	
	Searchmessage(String userid) throws ClassNotFoundException, SQLException{
		setUserid(userid);				//得到查找人的id
		searchmessage(this.admin);		
	}
	
	
	void searchmessage(Admin admin) throws ClassNotFoundException, SQLException {
		
		//连接数据库
		String sql="select name,sex,age,birthday from admin where id = ?";
    	Class.forName(admin.getDriver());
    	
    	Connection conn = DriverManager.getConnection(admin.getUrl(), admin.getUser(), admin.getSqlpassword());
    	PreparedStatement ps = conn.prepareStatement(sql);
    	ps.setString(1, this.userid);
    	ResultSet rs = ps.executeQuery();
    	
    	//通过查询的id获得他人的信息
    	while(rs.next()){
	    	name = rs.getString("name");
	    	sex = rs.getString("sex");
	    	age = rs.getString("age");
	    	birthday = rs.getString("birthday");
	    }
    	
    	JFrame frame = new JFrame("用户信息");
        frame.setLayout(null);
		
        JLabel nameStr = new JLabel("用户名:");
        nameStr.setBounds(250, 150, 100, 25);
        frame.add(nameStr);
        
        JLabel IDStr = new JLabel("账号:");
        IDStr.setBounds(250, 200, 100, 25);
        frame.add(IDStr);
        
        JLabel Sex = new JLabel("性别:");
        Sex.setBounds(250, 250, 100, 25);
        frame.add(Sex);
        
        JLabel Age = new JLabel("年龄:");
        Age.setBounds(250, 300, 100, 25);
        frame.add(Age);
        
        JLabel Birthday = new JLabel("出生日期:");
        Birthday.setBounds(245, 350, 100, 25);
        frame.add(Birthday);
        
        JLabel Myname = new JLabel(name);
        Myname.setBounds(300, 150, 150, 25);
        frame.add(Myname);
        
        JLabel MyID = new JLabel(userid);
        MyID.setBounds(300, 200, 150, 25);
        frame.add(MyID);
        
        JLabel MySex = new JLabel(sex);
        MySex.setBounds(300, 250, 150, 25);
        frame.add(MySex);
        
        JLabel MyAge = new JLabel(age);
        MyAge.setBounds(300, 300, 150, 25);
        frame.add(MyAge);
        
        JLabel MyBirthday = new JLabel(birthday);
        MyBirthday.setBounds(300, 350, 150, 25);
        frame.add(MyBirthday);
        
        frame.setBounds(300, 100, 800, 640);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    	
    	
	}
	
	
}
