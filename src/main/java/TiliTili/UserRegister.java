package TiliTili;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class UserRegister extends JFrame{
	UserRegister () {
		init();
	}
	void init() {
            final JFrame frame = new JFrame("注册管理员账号");
            frame.setLayout(null);
            
            JLabel nameStr = new JLabel("用户名:");
            nameStr.setBounds(250, 100, 100, 25);
            frame.add(nameStr);
        
            JLabel IDStr = new JLabel("账号:");
            IDStr.setBounds(250, 150, 100, 25);
            frame.add(IDStr);

            JLabel passwordStr = new JLabel("密码:");
            passwordStr.setBounds(250, 200, 100, 25);
            frame.add(passwordStr);  
               
            JLabel confrimStr = new JLabel("确认密码:");
            confrimStr.setBounds(250, 250, 100, 25);
            frame.add(confrimStr);
            
            JLabel sexStr = new JLabel("性别:");
            sexStr.setBounds(250, 300, 100, 25);
            frame.add(sexStr);
            
            JLabel ageStr = new JLabel("年龄:");
            ageStr.setBounds(250, 350, 100, 25);
            frame.add(ageStr);
            
            JLabel birthdayStr =new JLabel("生日:");
            birthdayStr.setBounds(250, 400, 100, 25);
            frame.add(birthdayStr);
            
            final JTextField userName = new JTextField();
            userName.setBounds(320, 100, 150, 25);
            frame.add(userName);

            final JTextField userID = new JTextField();
            userID.setBounds(320, 150, 150, 25);
            frame.add(userID);

            final JPasswordField password = new JPasswordField();
            password.setBounds(320, 200, 150, 25);
            frame.add(password);

            final JPasswordField confrimPassword = new JPasswordField();
            confrimPassword.setBounds(320, 250, 150, 25);
            frame.add(confrimPassword);
            
            final JTextField userSex = new JTextField();
            userSex.setBounds(320, 300, 150, 25);
            frame.add(userSex);
            
            final JTextField userAge = new JTextField();
            userAge.setBounds(320, 350, 150, 25);
            frame.add(userAge);
            
            final JTextField userBirthday = new JTextField();
            userBirthday.setBounds(320, 400, 150, 25);
            frame.add(userBirthday);
            
            JButton buttonregister = new JButton("注册");
            buttonregister.setBounds(350, 500, 70, 25);
            frame.add(buttonregister);
            


            frame.setBounds(400, 100, 800, 640);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
          //为注册按钮增加监听器
            buttonregister.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String name = userName.getText();
                    String ID = userID.getText();
                    String passwd = new String (password.getPassword());
                    String confrimpasswd = new String (confrimPassword.getPassword());
                    String sex = userSex.getText();
                    String age = userAge.getText();
                    String birthday = userBirthday.getText();
                    
                    //创建Register类
                    Register register = new Register();
                    register.setID(ID);
                    register.setName(name);
                    register.setPassword(passwd);
                    register.setconfirmpasswd(confrimpasswd);
                    register.setSex(sex);
                    register.setAge(age);
                    register.setBirthday(birthday);
                  
                    
                    //如果注册成功，返回登录界面

						try {
							if(register.JudgeRegister()) {

							    frame.setVisible(false);
							    LoginRegister loginregister = new LoginRegister();
							}
						} catch (ClassNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}


                }
                
            });
	}
}
