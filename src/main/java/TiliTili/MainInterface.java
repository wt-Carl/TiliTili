package TiliTili;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;

public class MainInterface extends JFrame{
	
	MainInterface(String myid){
		go(myid);
	}

	private void go(final String myid) {
        JFrame frame = new JFrame("TiliTili");
        frame.setLayout(null);
        
        JLabel nameStr = new JLabel("查看他人:");
        nameStr.setBounds(50, 50, 100, 25);
        frame.add(nameStr);
        
        final JTextField userID = new JTextField();
        userID.setBounds(110, 50, 100, 25);
        frame.add(userID);
        
        JButton buttonSearch = new JButton("搜索");
        buttonSearch.setBounds(210, 50, 70, 25);
        frame.add(buttonSearch);
        
        JButton buttonMymessage = new JButton("查看个人信息");
        buttonMymessage.setBounds(300, 150, 150, 40);
        frame.add(buttonMymessage);
        
        JButton buttonSpace = new JButton("吃瓜空间");
        buttonSpace.setBounds(300, 250, 150, 40);
        frame.add(buttonSpace);  
        
        JButton buttonMySpace = new JButton("我的空间");
        buttonMySpace.setBounds(300, 350, 150, 40);
        frame.add(buttonMySpace);  
        
        frame.setBounds(300, 100, 800, 640);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        //为查找键添加监听器
        buttonSearch.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String userid = userID.getText();
				try {
					Searchmessage searchmessage = new Searchmessage(userid);
				} catch (ClassNotFoundException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (SQLException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
			}
        	
        	
        	
        });
        //为个人信息添加监听器
        buttonMymessage.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
       		 	try {
					Mymessage mymessage = new Mymessage(myid);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}

        	
        });
        //为空间添加监听器
        buttonSpace.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					Space space = new Space(myid);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (BadLocationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
 
        });
        //为我的空间增添监听器
        buttonMySpace.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					MySpace myspace = new MySpace(myid);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (BadLocationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
        	
        	
        	
        });
	}
}