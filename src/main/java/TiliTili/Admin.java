package TiliTili;

public class Admin {
	static private String id;              //编号
	static private String name;           //姓名
	static private String password;      //密码
	static private String sex;			//性别
	static private String age;			//年龄
	static private String birthday;		//生日
	static private String driver = "com.mysql.cj.jdbc.Driver";
    static private String url = "jdbc:mysql://localhost:3306/test?serverTimezone=UTC";
    static private String user = "root";
    static private String sqlpassword = "123456";
	
	void setID(String id) {
	    this.id=id;
	}
	void setName(String name) {
	    this.name=name;
	}
	void setPassword(String password) {
	    this.password=password;
	}
	
	void setSex(String sex) {
	    this.sex=sex;
	}
	void setAge(String age) {
	    this.age=age;
	}
	void setBirthday(String birthday) {
	    this.birthday=birthday;
	}
	
	String getID() {
	    return this.id;
	}
	String getName() {
	    return this.name;
	}
	String getPassword() {
	    return this.password;
	}
	
	String getSex() {
	    return this.sex;
	}
	String getAge() {
	    return this.age;
	}
	String getBirthday() {
	    return this.birthday;
	}
	String getDriver() {
	    return driver;
	}
	String getUrl() {
	    return url;
	}
	String getUser() {
	    return user;
	}
	String getSqlpassword() {
	    return sqlpassword;
	}

}