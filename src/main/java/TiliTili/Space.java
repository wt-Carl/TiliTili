package TiliTili;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

 
public class Space extends JFrame {
	 private JScrollPane scrollPane = null;
	 private JTextPane text = null;
	 private Box box = null; // 放输入组件的容器
	 private JButton buttonInsert = null; // 插入按钮
	 private JTextField addText = null; // 文字输入框
	 private JComboBox fontSize = null; // 字体名称;字号大小;文字样式;文字颜色;文字背景颜色
	 
	 private StyledDocument doc = null;
	 private String myid;
	 private String myname;
	 Admin admin = new Admin();
	 void setMyid(String myid) {
		    this.myid=myid;
		}
		
	 String getMyid() {
		    return this.myid;
		}

		
	 Space(String myid) throws ClassNotFoundException, SQLException, BadLocationException{
		    setMyid(myid);
			setMyname(myid, this.admin);
			goit();
		}
 
 void goit() throws ClassNotFoundException, BadLocationException, SQLException {
	  try { // 使用Windows的界面风格
	   UIManager
	     .setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	  } catch (Exception e) {
	   e.printStackTrace();
	  }
	 
	  text = new JTextPane();
	  text.setEditable(false);
	  doc = text.getStyledDocument(); // 获得JTextPane的Document
	  scrollPane = new JScrollPane(text);
	  scrollPane.setPreferredSize(new Dimension(400, 400));
	  addText = new JTextField(18);
	  
	  String[] str_Size = { "12", "14", "18", "22", "30", "40" };
	  
	  
	  fontSize = new JComboBox(str_Size); // 字号
	
	  buttonInsert = new JButton("发布"); // 插入
	  
	 
	  buttonInsert.addActionListener(new ActionListener() { // 插入文字的事件
  public void actionPerformed(ActionEvent e) {
	    try {
			insert(getFontAttrib());
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    addText.setText("");
   }
  });
 
  
  
	  box = Box.createVerticalBox(); // 竖结构
	  Box box1 = Box.createHorizontalBox(); // 横结构
	  Box box2 = Box.createHorizontalBox(); // 横结构
	  box.add(box1);
	  box.add(Box.createVerticalStrut(8)); // 两行的间距
	  box.add(box2);
	  box.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8)); // 8个的边距
	  // 开始将所需组件加入容器
	 
	  box1.add(new JLabel("字号："));
	  box1.add(fontSize);
	  box1.add(Box.createHorizontalStrut(8));
	
	  box2.add(addText);
	  box2.add(Box.createHorizontalStrut(8));
	  box2.add(buttonInsert);
	  box2.add(Box.createHorizontalStrut(8));
	
	  this.getRootPane().setDefaultButton(buttonInsert); // 默认回车按钮
	  this.getContentPane().add(scrollPane);
	  this.getContentPane().add(box, BorderLayout.SOUTH);
	  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 
	  pack();
	  this.setLocationRelativeTo(null);
	  this.setVisible(true);
	  doc.insertString(doc.getLength(), getMysql(this.admin) + "\n", null);
	  addText.requestFocus();
 }
 
 
 //发布事件
 private void insert(FontAttrib attrib) throws ClassNotFoundException, SQLException {
	  try { // 插入文本
		 doc.insertString(doc.getLength(), attrib.getText() + "\n",
		 attrib.getAttrSet());
		 toMysql(this.myid,this.myname,attrib.getText(),this.admin);
	  } catch (BadLocationException e) {
		 e.printStackTrace();
	  }
 }
 
 //获得文本
 private FontAttrib getFontAttrib() {
	  FontAttrib att = new FontAttrib();
	  att.setText(addText.getText());
	  att.setSize(Integer.parseInt((String) fontSize.getSelectedItem()));
	  
	  return att;
 }
 
 
 
 //通过myid得到myname
void setMyname(String myid, Admin admin) throws ClassNotFoundException, SQLException {
	 String sql="select name from admin where id = ?";
 	Class.forName(admin.getDriver());
 	
 	Connection conn = DriverManager.getConnection(admin.getUrl(), admin.getUser(), admin.getSqlpassword());
	PreparedStatement ps = conn.prepareStatement(sql);
	ps.setString(1, myid);
	ResultSet rs = ps.executeQuery();
	while(rs.next()) {
		myname = rs.getString("name");
	}

	this.myname = myname;
 }
 
 String getMyname(String myname) {
		return this.myname;
	}
 
 
 //将数据传入到数据库
 private void toMysql(String myid, String myname,String thistext, Admin admin) throws ClassNotFoundException, SQLException {
	 String sql="insert into friend (id, name, text)values (?,?,?)";
 	 Class.forName(admin.getDriver());
	 
 	Connection conn = DriverManager.getConnection(admin.getUrl(), admin.getUser(), admin.getSqlpassword());
	PreparedStatement ps = conn.prepareStatement(sql);
	ps.setString(1, myid);
    ps.setString(2, this.myname);
	ps.setString(3, thistext);
 }
 
 //从数据库获得数据并插入到朋友圈当中
 private String getMysql(Admin admin) throws ClassNotFoundException, SQLException {
	String sql="select name,text from friend";
     
 	Class.forName(admin.getDriver());
 	Connection conn = DriverManager.getConnection(admin.getUrl(), admin.getUser(), admin.getSqlpassword());
 	PreparedStatement ps = conn.prepareStatement(sql);
 	ResultSet rs = ps.executeQuery();
 	
 	ArrayList<Text> container = new ArrayList<Text>();
 	while(rs.next()) {
 		
 		String texts = rs.getString("text");
 		String names = rs.getString("name");
 		
 		Text text = new Text();
 		text.setName(names);
 		text.setText(texts);
 		
 		container.add(text);

 	}
 	
 	int i;
 	String data = "";
 	for(i=container.size()-1;i>=0;i++) {
 		data = data+container.get(i).toString();
 	}
 	
	 return data;
 }
 
 
 
 
 
 public class FontAttrib {
	  public static final int GENERAL = 0; // 常规
	  public static final int BOLD = 1; // 粗体
	  public static final int ITALIC = 2; // 斜体
	  public static final int BOLD_ITALIC = 3; // 粗斜体
	  private SimpleAttributeSet attrSet = null; // 属性集
	  private String text = null, name = null; // 要输入的文本和字体名称
	  private int style = 0, size = 0; // 样式和字号
	  private Color color = null, backColor = null; // 文字颜色和背景颜色
 
  
  public FontAttrib() {
  }
 
  public SimpleAttributeSet getAttrSet() {
	   attrSet = new SimpleAttributeSet();
	   if (name != null) {
		   StyleConstants.setFontFamily(attrSet, name);
	   }
	   if (style == FontAttrib.GENERAL) {
		    StyleConstants.setBold(attrSet, false);
		    StyleConstants.setItalic(attrSet, false);
	   } else if (style == FontAttrib.BOLD) {
		    StyleConstants.setBold(attrSet, true);
		    StyleConstants.setItalic(attrSet, false);
	   } else if (style == FontAttrib.ITALIC) {
		    StyleConstants.setBold(attrSet, false);
		    StyleConstants.setItalic(attrSet, true);
	   } else if (style == FontAttrib.BOLD_ITALIC) {
		    StyleConstants.setBold(attrSet, true);
		    StyleConstants.setItalic(attrSet, true);
	   }
	   StyleConstants.setFontSize(attrSet, size);
	   if (color != null) {
		   StyleConstants.setForeground(attrSet, color);
	   }
	   if (backColor != null) {
		   StyleConstants.setBackground(attrSet, backColor);
	   }
	   return attrSet;
}
  
  
 
  public void setAttrSet(SimpleAttributeSet attrSet) {
	  this.attrSet = attrSet;
  }
  
 
  public String getText() {
	  return text;
  }
 
  public void setText(String text) {
	  this.text = text;
  }
 
 
  public int getSize() {
	  return size;
  }
 
  public void setSize(int size) {
	  this.size = size;
  }

  
 }
 
}